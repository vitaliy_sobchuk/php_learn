<?php
	class AddInfo{

		private $pdo;

		public function __construct(){

			$dbconfig = include "db_config.php";

			$dsn='mysql:host='.$dbconfig['host'].';dbname='.$dbconfig['database'];
		
			$pdo = new PDO(
				$dsn,
				$dbconfig['user'],
				$dbconfig['pass'],
				$dbconfig['options']
			);

			$this->pdo=$pdo;

		}
		function Reg(){

			if(isset($_POST['button'])){
				$data = $this->pdo->query('SELECT user, name, surname, dob FROM info WHERE user="'.$_SESSION['username'].'"')->fetchAll();

				if(strlen($_POST['name']) > '1'){
					
					$allowed = array("name");
					$sql ="UPDATE info SET  name = '".$_POST['name']."' WHERE user = '".$_SESSION['username']."'";
					$stm = $this->pdo->prepare($sql);
					$stm->execute();
					echo 'You added your name'; ?><br><?php
				}

				if(strlen($_POST['surname']) > '1'){

					$allowed = array("surname");
					$sql ="UPDATE info SET surname = '".$_POST['surname']."' WHERE user = '".$_SESSION['username']."'";
					$stm = $this->pdo->prepare($sql);
					$stm->execute();
					echo 'You added your surname'; ?><br><?php
				}
				if(strlen($_POST['dob']) > '1'){

					$allowed = array("dob");
					$sql ="UPDATE info SET dob = '".$_POST['dob']."' WHERE user = '".$_SESSION['username']."'";
					$stm = $this->pdo->prepare($sql);
					$stm->execute();
					echo 'You added your Date Of Birth'; ?><br><?php
				}
				if(strlen($_POST['email']) > '1'){
					$data2 = $this->pdo->query('SELECT id FROM users WHERE user="'.$_SESSION['username'].'"')->fetchAll();
					$allowed = array("email,user_id");
					$sql ="INSERT INTO emails(email,user_id) VALUES ('".$_POST['email']."','".$data2[0]['id']."')";
					$stm = $this->pdo->prepare($sql);
					$stm->execute();
					echo 'You added your email'; ?><br><?php
				}

			}else{
			echo 'You didn`t wrote any information about yourself';	
			}
		}	
	}

	function Info(){

	$a=new AddInfo;
		if(isset($_POST["button"])){
			$a->Reg();
		}elseif(isset($_GET['exit'])){
		    header("Location: logout.php");
		    exit;

		}else{
?>
<p>Hello User, do you want to add aditional information about yourself?</p><br>
<form method="POST">
	<input type="text" name="name" placeholder="Name" maxlength="100" size="30"><br>
	<input type="text" name="surname" placeholder="Surname" maxlength="100" size="30"><br>
	<input type="text" name="dob" placeholder="Date of Birth" maxlength="100" size="30"><br>
	<input type="text" name="email" placeholder="Email" maxlength="100" size="30"><br>
	<input type="submit" name="button"><br>
</form>	
	<a href="?exit">Exit</a>
<?php
			
		}
	}	

	session_start();
	include "expire.php";
	Guard2();
	Expire();
	
	if ($guard='1'){
		Info();
		exit();

	}else{
		header("Location: logout.php");
		exit();
	}
?>