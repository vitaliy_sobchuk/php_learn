<?php

// This is the database connection configuration.
return array(
	
	'connectionString' => 'mysql:host=localhost;dbname=yii_learn',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	
);