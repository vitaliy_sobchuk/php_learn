<?php
	return array( 
		'host'=>'localhost',
		'database'=>'users',
		'charset'=>'',
		'user'=>'root',
		'pass'=>'',
		'options'=>[ 
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			] 
	);
?>