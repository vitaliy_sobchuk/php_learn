
<?php

	class User{
		private $pdo;

		private $isUserLoggedIn =false;
		private $isAdminLoggedIn =false;

		public function __construct(){

			$dbconfig = include "db_config.php";

			$dsn='mysql:host='.$dbconfig['host'].';dbname='.$dbconfig['database'];
		
			$pdo = new PDO(
				$dsn,
				$dbconfig['user'],
				$dbconfig['pass'],
				$dbconfig['options']
			);

			$this->pdo=$pdo;

		}
		function Session(){

			if(isset($_POST['logn']) && isset($_POST['pass'])){
				session_start();
				include "expire.php";
				Expire();

				if($this->isAdminLoggedIn==true){
					$_SESSION['username'] = $_POST['logn'];
					$_SESSION['isAdmin'] = $this->isAdminLoggedIn;
					if(isset($_POST['stayIn'])){
						$_SESSION['stayIn']=true;
					}
					header('Location:admin.php');
					exit();
				}
				if($this->isUserLoggedIn==true){
					$_SESSION['username'] = $_POST['logn'];
					if(isset($_POST['stayIn'])){
						$_SESSION['stayIn']=true;
					}
					header('Location:user.php');
					exit();
				}
			}
		}


		function Auth(){
			if(isset($_POST['logn'])) {
				$data = $this->pdo->query('SELECT user, pass, isAdmin FROM users WHERE user="'.$_POST['logn'].'"')->fetchAll();

				if(isset($data[0]['user']) && $_POST['pass']==$data[0]['pass'] && $data[0]['isAdmin']=="1"){
					$this->isAdminLoggedIn =true;
					$this->Session();
					exit();	
				}
				elseif(isset($data[0]['user']) && $_POST['pass']==$data[0]['pass']){
					$this->isUserLoggedIn =true;
					$this->Session();
					exit();
				}else{
					echo "Authorization unsuccesful, you aint my homie.";
				}
			}
		}
		function isUserGuest(){
			return $this->isUserLoggedIn;
		}
		function isUserAdmin(){
			return $this->isAdminLoggedIn;
		}
	}	
	function Main(){
		$a=new User;
		if(isset($_POST["logn"])){
			$a->Auth();
		}else{
?>
			<form method="POST">
				<input type="text" name="logn" placeholder="Login" maxlength="10">
				<input type="password" name="pass" placeholder="Password" maxlength="10">
				<input type="checkbox" name="stayIn">Remember Me
				<input type="submit">
			</form>	
<?php
		}
	}
	Main();	
?>

