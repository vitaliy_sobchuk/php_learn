<?php

// This is the database connection configuration.
return array(
	
	'connectionString' => 'mysql:host=localhost;dbname=project_1',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
);