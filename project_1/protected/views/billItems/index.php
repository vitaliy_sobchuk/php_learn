<?php
/* @var $this BillItemsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bill Items',
);

$this->menu=array(
	array('label'=>'Адміністрування рахунків', 'url'=>array('bill/admin')),
	array('label'=>'Список Рахунків', 'url'=>array('bill/index')),
	
);
?>

<h1>Керування рахунками</h1>

<?php 
if(isset(Yii::app()->session['cart']))
		{
?>

<div class="form">
<?php echo CHtml::beginForm(); ?>
	<?php
	
			$itemIds=explode(',', Yii::app()->session['cart']);
			unset($itemIds[0]);
	?>

<?php

	foreach ($itemIds as $itemId) { 
?>
<table>
	<tr><th>Назва товару</th><th>Кількість</th></tr>
	<tr>
		<td>
		<?php 
		$name=Items::model()->findbyPk($itemId);
		echo $name['name'];
		 ?>
		</td>

		<td>
		<div class='amount'>
		<?php
		echo CHtml::TextField($itemId);
		?>
		</div>
		</td>
		<br>
	</tr>
	</table>

<?php
	}
		
?>
	<br>
		<b><?php echo 'Введіть назву рахунку'; ?>:</b>
	<div class='name'>
	<?php echo CHtml::TextField('name');
	?>
	</div>
<?php
	echo CHtml::submitButton('Створити рахунок', 
	    	array(
	    		'submit'=>array('bill/CreateBill'),
	    	)
	    );		
?>
<br>
<?php
	echo CHtml::submitButton('Очистити Корзинку', 
    	array(
    		'submit'=>array('DeleteBin'),
    	)
    );
?>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->
<?php
}
?>
