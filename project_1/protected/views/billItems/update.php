<?php
/* @var $this BillItemsController */
/* @var $model BillItems */

$this->breadcrumbs=array(
	'Bill Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BillItems', 'url'=>array('index')),
	array('label'=>'View BillItems', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BillItems', 'url'=>array('admin')),
);
?>

<h1>Update BillItems <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>