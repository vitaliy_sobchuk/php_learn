<?php
/* @var $this BillController */
/* @var $model Bill */

$this->breadcrumbs=array(
	'Bills'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список Рахунків', 'url'=>array('index')),
	array('label'=>'Змінити данні у рахунку', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Видалити Рахунок', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Адміністрування Рахунків', 'url'=>array('admin')),
);
?>

<h1>Данні рахунку # <?php echo $model->id; ?></h1>

<?php 
/*
 $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'price',
	),
)); 
*/
?>
<?php
$bill=Bill::model()->findByPk([
				$model->id
			]);
$billItems=billItems::model()->findAllByAttributes([
				'bill_id'=>$model->id,
			]);
?>

<table>
	<tr><th>Назва рахунку</th></tr>
	<tr>
		<td>
		<?php 
		echo $bill->name;
		 ?>
		</td>
	</tr>
	<tr><th>Price</th></tr>
	<tr>
		<td>
		<?php 
		echo $bill->price;
		 ?>
		</td>
	</tr>

<?php
	foreach ($billItems as $item) {
?>

		<table>
	<tr><th>Назва товару</th><th>Кількість</th></tr>
	<tr>
		<td>
		<?php 
		echo $item->item->name;
		 ?>
		</td>

		<td>
		<?php
		echo $item->amount;
		?>
		</td>
		<br>
	</tr>
		</table>
<?php
	}

?>
</table>
