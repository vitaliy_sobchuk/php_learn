<?php
/* @var $this BillController */
/* @var $model Bill */

$this->breadcrumbs=array(
	'Bills'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список Рахунків', 'url'=>array('index')),
	array('label'=>'Інформація про рахунок', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Адміністрування Рахунків', 'url'=>array('admin')),
);
?>

<h1>Змінити данні у рахунку: <?php echo $model->id; ?></h1>
<div class="form">

<?php echo CHtml::beginForm(); ?>

	<b>Назва рахунку<b/>
	<div class='name'>
	<?php echo CHtml::TextField('name',$value=$model->name);
	?>
	</div>

<?php
	
			$items=BillItems::model()->findAllByAttributes([
				'bill_id'=>$model->id,
			]);
	?>
	<?php
	foreach ($items as $item) {
	// $item->item->name
	// $item->item->price
?>
<table>
	<tr><th>Назва товару</th><th>Кількість</th></tr>
	<tr>
		<td>
		<?php 
		echo $item->item->name;
		 ?>
		</td>

		<td>
		<div class='amount'>
		<?php
		echo CHtml::TextField($item->id,$value=$item->amount);
		?>
		</div>
		</td>
		<br>
	</tr>
	</table>

<?php 
	}
	echo CHtml::hiddenField('bill_id',$value=$model->id);
?>



<?php
	echo CHtml::submitButton('Змінити данні', 
    	array(
    		'submit'=>array('UpdateBill'),
    	)
    );
?>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->