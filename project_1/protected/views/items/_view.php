<?php
/* @var $this ItemsController */
/* @var $data Items */
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

<div class="view">
	<?php 
		
		$isAdmin=Users::model()->findByAttributes([
			'username'=>Yii::app()->user->name
		]);
		if ($isAdmin['isAdmin']=='1'){
?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />
<?php
		}
?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

    <?php 
    	echo CHtml::button('Додати до рахунку', 
	    	array(
	    		'submit'=>array('addToBill','item_id'=>$data->id),
	    		// 'submit'=>Yii::app()->createUrl('/items/addToBill',array('item_id'=>$data->id)),
	    	)
    	); ?>
    <br />

</div>