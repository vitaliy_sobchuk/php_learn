<?php
/* @var $this ItemsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Items',
);

$isAdmin=Users::model()->findByAttributes([
			'username'=>Yii::app()->user->name
		]);
		if ($isAdmin['isAdmin']=='1'){
$this->menu=array(
	array('label'=>'Create Items', 'url'=>array('create')),
	array('label'=>'Manage Items', 'url'=>array('admin')),
);
}
?>

<h1>Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
	if (isset(Yii::app()->session['billItems']))
		CVarDumper::dump(Yii::app()->session['billItems'], 3, true);
?>