<?php

class BillController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('CreateBill','UpdateBill'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bill;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bill']))
		{
			$model->attributes=$_POST['Bill'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bill']))
		{
			$model->attributes=$_POST['Bill'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$billItem=billItems::model()->findAllByAttributes([
				'bill_id'=>$id,
			]);
		foreach ($billItem as $item) {
			$item->delete();
		}
		$this->loadModel($id)->delete();
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Bill');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bill('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bill']))
			$model->attributes=$_GET['Bill'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bill the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bill::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bill $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bill-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionCreateBill()
	{
		/*
		$items=array();
		$itemsIds=$_POST;
		unset($itemsIds['name']);
			foreach ($itemsIds as $itemId=>$amount) {
		}
		*/

		$price=0;
		$bill=new Bill;
		$bill->name=$_POST['name'];
		$bill->price=$price;
		$bill->user_name=Yii::app()->user->id;
		$bill->save();

		$itemIds=$_POST;
		unset($itemIds['name']);
			foreach ($itemIds as $itemId=>$amount) {
				$amount=$_POST[$itemId];
				$item=Items::model()->find('id='.$itemId);
				$priceF1=$item->price;
				$priceForAll=$priceF1*$amount;
				$price=$price+$priceForAll;
				$billItem=new BillItems;
				$billItem->item_id=$itemId;
				$billItem->amount=$amount;
				$billItem->bill_id=$bill->id;
				$billItem->save();
			}
		
		$bill->price=$price;
		$bill->save();
		$this->redirect(['index']);
		return true;
	}
	public function actionUpdateBill()
	{
		if($_POST['name']!=''){
			$bill=Bill::model()->findByPk($_POST['bill_id']);
			$bill->name=$_POST['name'];
			$bill->save();
		}
		unset($_POST['name']);
		unset($_POST['bill_id']);
		$price=0;
		foreach ($_POST as $id => $amount) 
			{
				$billItem=BillItems::model()->findByPk($id);
				$billItem->amount=$amount;
				$billItem->save();
				$item=Items::model()->findByPk($billItem->item_id);
				$priceF1=$item->price;
				$priceForAll=$priceF1*$amount;
				$price=$price+$priceForAll;

			}
		$bill->price=$price;
		$bill->save();
		$this->redirect(['index']);
		
	}
}
