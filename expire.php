<?php

    function Expire(){

        if(!isset($_SESSION['stayIn'])){

            $timeout = 120; // Number of seconds until it times out.
         
            // Check if the timeout field exists.
            if(isset($_SESSION['timeout'])) {
            // See if the number of seconds since the last
            // visit is larger than the timeout period.
                $duration = time() - (int)$_SESSION['timeout'];
                if($duration > $timeout) {
                    header("Location: logout.php");
                }
            }
            $_SESSION['timeout'] = time();
        }
    }

    $guard='';
    function Guard1(){
        if(!isset($_SESSION['username']) && !isset($_SESSION['isAdmin'])){
        header("Location: logout.php");
        }else{
            return $guard='1';
        }
    }
    function Guard2(){
        if(!isset($_SESSION['username'])){
        header("Location: logout.php");
        }else{
            return $guard='1';
        }
    }
?>
