<?php
	session_start();
	unset($_SESSION['username']);
	unset($_SESSION['isAdmin']);
	unset($_SESSION['start']);
	session_destroy();
	header('Location:http://localhost/php_learn/index.php');
?>